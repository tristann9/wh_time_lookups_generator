package com.teintel.wh_time_lookup_generator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by teveritt on April 04, 2015.
 */
public class TimeLookupGenerator {

    private String basePath = "build" + File.separator;
    private int startYear = 2014;
    private int endYear = startYear + 50;


    public TimeLookupGenerator() {
    }

    public TimeLookupGenerator(int startYear, int endYear) {
        this.startYear = startYear;
        this.endYear = endYear;
    }

    public static void main(String args[]) throws IOException {
        Locale.setDefault(new Locale("en", "IE"));
        TimeLookupGenerator generator = new TimeLookupGenerator();
        generator.generateWeeks();
        generator.generateYears();
        generator.generateDays();
        generator.generateMonths();
        generator.generateQuarters();
    }

    public void generateYears() throws IOException {
        String tableName = "LU_YEAR";

        File csvFile = new File(basePath + tableName + ".csv");
        File sqlFile = new File(basePath + tableName + ".sql");
        String headers = "year_id";

        FileUtils.deleteQuietly(csvFile);
        FileUtils.deleteQuietly(sqlFile);

        boolean printHeaders = true;

        for (int yearId = startYear; yearId <= endYear; yearId++) {
            String sqlOutput = String.format("INSERT INTO %s (%s) " +
                    "values (%s);", tableName, headers, yearId);
            FileUtils.writeStringToFile(sqlFile, sqlOutput + "\n", true);
            if (printHeaders) {
                FileUtils.writeStringToFile(csvFile, headers + "\n", true);
                printHeaders = false;
            }

            FileUtils.writeStringToFile(csvFile, yearId + "\n", true);
        }
    }

    /**
     * All weeks are starting on Monday and ending on Sunday (ISO-8601)
     */
    public void generateWeeks() throws IOException {
        String tableName = "LU_WEEK";

        File csvFile = new File(basePath + tableName + ".csv");
        File sqlFile = new File(basePath + tableName + ".sql");
        String headers = "week_id,year_id,week_desc,week_of_year_id";

        FileUtils.deleteQuietly(csvFile);
        FileUtils.deleteQuietly(sqlFile);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, 1);
        calendar.set(Calendar.YEAR, startYear);

        boolean printHeaders = true;

        int yearId = startYear;
        while (yearId <= endYear) {

            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            Date monday = calendar.getTime();

            String week_desc = String.format("Week of %s", DateFormatUtils.format(monday, "MMM dd yyyy"));
            yearId = calendar.get(Calendar.YEAR);
            int weekOfYearId = calendar.get(Calendar.WEEK_OF_YEAR);
            if (weekOfYearId == 1) {
                Calendar weekCal = Calendar.getInstance();
                weekCal.setTime(calendar.getTime());
                weekCal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                yearId = weekCal.get(Calendar.YEAR);
            }

            int weekId = yearId * 100 + calendar.get(Calendar.WEEK_OF_YEAR);

            String week_desc2 = String.format("%s-%s", weekOfYearId, yearId);

            //String output = String.format("%s, %s, %s, %s, %s", weekId, yearId, week_desc, week_desc2, weekOfYearId);
            String csvOutput = String.format("%s,%s,%s,%s", weekId, yearId, week_desc, weekOfYearId);
            String sqlOutput = String.format("INSERT INTO %s (%s) " +
                    "values (%s,%s,'%s',%s);", tableName, headers, weekId, yearId, week_desc, weekOfYearId);

            FileUtils.writeStringToFile(sqlFile, sqlOutput + "\n", true);
            if (printHeaders) {
                FileUtils.writeStringToFile(csvFile, "week_id,year_id,week_desc,week_of_year_id\n", true);
                printHeaders = false;
            }

            FileUtils.writeStringToFile(csvFile, csvOutput + "\n", true);

            calendar.add(Calendar.WEEK_OF_YEAR, 1);
        }
    }

    public void generateDays() throws IOException {
        String tableName = "LU_DAY";

        File csvFile = new File(basePath + tableName + ".csv");
        File sqlFile = new File(basePath + tableName + ".sql");
        String headers = "day_id,day_desc,day_of_week_id,day_of_month_id,date_id,week_id,week_of_year_id,month_id,month_of_year_id,quarter_id,quarter_of_year_id,year_id";

        FileUtils.deleteQuietly(csvFile);
        FileUtils.deleteQuietly(sqlFile);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, 1);
        calendar.set(Calendar.YEAR, startYear);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        boolean printHeaders = true;

        int yearId = startYear;
        while (yearId <= endYear) {
            Date date = calendar.getTime();
            yearId = calendar.get(Calendar.YEAR);

            int monthOfYear = calendar.get(Calendar.MONTH) + 1;
            int monthId = yearId * 100 + monthOfYear;

            int dayId = yearId * 10000 + (monthOfYear * 100) + calendar.get(Calendar.DAY_OF_MONTH);
            //int dayId = yearId * 1000 + calendar.get(Calendar.DAY_OF_YEAR);
            String dayDesc = DateFormatUtils.format(date, "EEEE, MMMM d, yyyy");
            int dayOfWeekId = calendar.get(Calendar.DAY_OF_WEEK);
            switch (dayOfWeekId) {
                case Calendar.SUNDAY:
                    dayOfWeekId = 7;
                    break;
                case Calendar.SATURDAY:
                    dayOfWeekId = 6;
                    break;
                default:
                    dayOfWeekId -= 1;
            }
            int dayOfMonthId = calendar.get(Calendar.DAY_OF_MONTH);
            String dateId = DateFormatUtils.format(date, "yyyy-MM-dd");

            int weekId = yearId * 100 + calendar.get(Calendar.WEEK_OF_YEAR);
            int weekOfYearId = calendar.get(Calendar.WEEK_OF_YEAR);
            if (weekOfYearId == 1) {
                Calendar weekCal = Calendar.getInstance();
                weekCal.setTime(calendar.getTime());
                weekCal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                yearId = weekCal.get(Calendar.YEAR);
            }


            int quarterOfYearId = getQuarterId(calendar.get(Calendar.MONTH));
            int quarterId = yearId * 100 + quarterOfYearId;

            int dayOfYearId = calendar.get(Calendar.DAY_OF_YEAR);

            String csvOutput = String.format("%s,\"%s\",%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                    dayId, dayDesc, dayOfWeekId, dayOfMonthId, dateId, weekId, weekOfYearId, monthId, monthOfYear, quarterId, quarterOfYearId, yearId);
            String sqlOutput = String.format("INSERT INTO %s (%s) " +
                            "values (%s,'%s',%s,%s,'%s',%s,%s,%s,%s,%s,%s,%s);",
                    tableName, headers, dayId, dayDesc, dayOfWeekId, dayOfMonthId, dateId, weekId, weekOfYearId, monthId, monthOfYear, quarterId, quarterOfYearId, yearId);

            FileUtils.writeStringToFile(sqlFile, sqlOutput + "\n", true);
            if (printHeaders) {
                FileUtils.writeStringToFile(csvFile, headers + "\n", true);
                printHeaders = false;
            }

            FileUtils.writeStringToFile(csvFile, csvOutput + "\n", true);

            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
    }

    public void generateMonths() throws IOException {
        String tableName = "LU_MONTH";

        File csvFile = new File(basePath + tableName + ".csv");
        File sqlFile = new File(basePath + tableName + ".sql");
        String headers = "month_id,year_id,quarter_id,quarter_of_year_id,month_of_year_id,month_desc";

        FileUtils.deleteQuietly(csvFile);
        FileUtils.deleteQuietly(sqlFile);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, 1);
        calendar.set(Calendar.YEAR, startYear);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        boolean printHeaders = true;

        int yearId = startYear;
        while (yearId <= endYear) {
            Date date = calendar.getTime();
            yearId = calendar.get(Calendar.YEAR);

            int monthId = yearId * 100 + calendar.get(Calendar.MONTH) + 1;
            String monthDesc = DateFormatUtils.format(date, "MMMM yyyy");
            int monthOfYear = calendar.get(Calendar.MONTH) + 1;


            int quarterOfYearId = getQuarterId(calendar.get(Calendar.MONTH));
            int quarterId = yearId * 100 + quarterOfYearId;

            String csvOutput = String.format("%s,%s,%s,%s,%s,\"%s\"",
                    monthId, yearId, quarterId, quarterOfYearId, monthOfYear, monthDesc);
            String sqlOutput = String.format("INSERT INTO %s (%s) " +
                            "values (%s,%s,%s,%s,%s,'%s');",
                    tableName, headers, monthId, yearId, quarterId, quarterOfYearId, monthOfYear, monthDesc);

            FileUtils.writeStringToFile(sqlFile, sqlOutput + "\n", true);
            if (printHeaders) {
                FileUtils.writeStringToFile(csvFile, headers + "\n", true);
                printHeaders = false;
            }

            FileUtils.writeStringToFile(csvFile, csvOutput + "\n", true);

            calendar.add(Calendar.MONTH, 1);
        }
    }

    public void generateQuarters() throws IOException {
        String tableName = "LU_QUARTER";

        File csvFile = new File(basePath + tableName + ".csv");
        File sqlFile = new File(basePath + tableName + ".sql");
        String headers = "quarter_id,year_id,quarter_of_year_id,quarter_desc";

        FileUtils.deleteQuietly(csvFile);
        FileUtils.deleteQuietly(sqlFile);

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, 1);
        calendar.set(Calendar.YEAR, startYear);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        boolean printHeaders = true;

        int yearId = startYear;
        while (yearId <= endYear) {
            yearId = calendar.get(Calendar.YEAR);

            int quarterOfYearId = getQuarterId(calendar.get(Calendar.MONTH));
            int quarterId = yearId * 100 + quarterOfYearId;
            String quarterDesc = String.format("Q%s %s", quarterOfYearId, yearId);

            String csvOutput = String.format("%s,%s,%s,\"%s\"",
                    quarterId, yearId, quarterOfYearId, quarterDesc);
            String sqlOutput = String.format("INSERT INTO %s (%s) " +
                            "values (%s,%s,%s,'%s');",
                    tableName, headers, quarterId, yearId, quarterOfYearId, quarterDesc);

            FileUtils.writeStringToFile(sqlFile, sqlOutput + "\n", true);
            if (printHeaders) {
                FileUtils.writeStringToFile(csvFile, headers + "\n", true);
                printHeaders = false;
            }

            FileUtils.writeStringToFile(csvFile, csvOutput + "\n", true);

            calendar.add(Calendar.MONTH, 3);
        }
    }

    private int getQuarterId(int month) {
        switch (month) {
            case Calendar.JANUARY:
            case Calendar.FEBRUARY:
            case Calendar.MARCH:
                return 1;
            case Calendar.APRIL:
            case Calendar.MAY:
            case Calendar.JUNE:
                return 2;
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.SEPTEMBER:
                return 3;
            case Calendar.OCTOBER:
            case Calendar.NOVEMBER:
            case Calendar.DECEMBER:
                return 4;
            default:
                return -1;
        }
    }
}
